# Contents

This repository contains different p≡p and other (e.g., MEDUP) 
Internet-Drafts, some already submitted in different versions, others under 
development and yet to be submitted in a first version -- to the IETF 
Internet-Drafts repository:

https://www.ietf.org/id-info/

In each directory an Internet-Draft (I-D) is provided, e.g.:

* pep (pEp's general draft)
* pep-email (pEp for email)
* pep-handshake (pEp's method to authenticate peers)
* pep-keysync (pEp's method to synchronize secret keys)
* pep-rating (pEp's rating system to signal privacy levels)
* pep-trustwords (pEp's replacement for fingerprints)

# Tools

The mandatory ASCII or XML files for submission are created using
the kramdown-rfc2629 tool creating the XMLv2 format initially described as 
RFC2629 (obsoleted by RFC7749):

https://tools.ietf.org/html/rfc7749

Please install kramdown-rfc2629, ruby and xml2rfc through your package 
system or directly from the respective sites:

* https://github.com/cabo/kramdown-rfc2629
* https://www.ruby-lang.org/
* https://xml2rfc.tools.ietf.org/

You can also install kramdown-rfc2629 through the ruby gem packaging system 
if your packaging system doesn't know anything about this tool:

https://rubygems.org/

# Structure and contents of each I-D

Each p≡p I-D consists of the following raw files:

* ${name_of_the_draft}.mkd
* Makefile

Abstracts should give a rough overview of what the I-D is about: this is to 
happen over 5-10 lines. Less than 3 and more than 20 lines should be avoided.

The main content goes into the middle section: make sure you also provide 
"Introduction" and "Security Consideration" subsections. For the latter, 
please check RFC3552 to see what aspects to consider:

https://tools.ietf.org/html/rfc3552

A back section can be used for appendix information like listings or 
information which provides additional information, but which is not 
mandatory to understand the I-D.

Basic metadata (e.g., authors' names, email addresses and the draft title), 
but also references (including RFCs, by instance) are made available through
a special syntax at the beginning of the Markdown file.

Makefiles contain the build instructions to generate the I-Ds in the required
ASCII and xml2rfc v2 forms accepted by the IETF submission system. Additionally,
they also create HTML versions.

By default, revision number (REV) is set to "00": make sure to increment this
as IETF contributions are furthered.

# Version Control System (git)

Most important commands:

## Clone repository

     git clone ssh://gitea@gitea.pep.foundation:23065/pEp.foundation/internet-drafts.git
     git clone https://gitea.pep.foundation/pEp.foundation/internet-drafts.git

## Update local directories to latest remote repository state

     cd internet-drafts
     git pull

Note: Merge handling may be required to take care of (if by git)

## Upload local changes to repository 

Important: To avoid conflicts, _always_ update the local directories
to the latest repository state (see commands in last bullet point)
_before_ doing a commit!

     git commit -m "<Changelog, short description of changes>" [ <filename> ]
     git push

## More information

* https://git-scm.com/
* https://github.github.com/training-kit/downloads/github-git-cheat-sheet/
