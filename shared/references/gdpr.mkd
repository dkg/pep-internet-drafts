  GDPR:
    target: https://eur-lex.europa.eu/eli/dir/2016/680/oj
    title: "General Data Protection Regulation 2016/680 of the European Parliament and of the Council (GDPR)."
#    author:
#      name: Nomen Necsio
#      ins: N. Nescio
    date: 2016-04-27
    seriesinfo:
      Official Journal of the European Union, L 119/89, 4.5.2016   

