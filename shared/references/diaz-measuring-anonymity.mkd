  Diaz:
#    target: 
    title: Towards Measuring Anonymity
    author:
      -
        name: Claudia Diaz
        ins: C. Diaz
      -
        name: Stefaan Seys
        ins: St. Seys
      -
        name: Joris Claessens 
        ins: J. Claessens 
      -
        name: Bart Preneel
        ins: B. Preneel
    date: 2002
    seriesinfo:
      PET: Privacy Enhancing Technologies, Second International Workshop, San Francisco, CA, USA, April 14-15, 2002, Revised Papers, pp. 54-68