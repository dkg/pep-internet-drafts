        +-------+          +-------+     +-----------+    +-----+
        | Alice |          |  TLA  |     | Bobs Corp |    | Bob |
        +-------+          +-------+     +-----------+    +-----+
            |                  ^               |             |

        ~~~~~~~~~~ TLA sees message from @ alice to @ bob ~~~~~~~~~
            |                  ^               |             |
            |                  ^               |             |
            | Message 5        ^               |             |
            | To: @ bob        ^               |             |
            | ----------------###------------> @             |
            |                  ^               @ ----------> |
            |                  ^               |             |
{: #pEp_F_5 title="Principle of eavesdropping messages in transit" artwork-align="center" }
