MIME-Version: 1.0
Content-Type: multipart/mixed; boundary="boundary2"

--boundary2
Content-Type: text/plain; charset="utf-8"
Content-Disposition: inline; filename="msg.txt"

pEp-Wrapped-Message-Info: OUTER

--boundary2
Content-Type: message/rfc822

Message-ID: <pEp.1234>
Date: Wed, 1 Jan 2020 23:23:23 +0200
Subject: Credentials
X-pEp-Version: 2.0
MIME-Version: 1.0
Content-Type: multipart/mixed; boundary="boundary3"

--boundary3
Content-Type: text/plain; charset="utf-8"
Content-Transfer-Encoding: quoted-printable

pEp-Wrapped-Message-Info: INNER

Dear Bob

Please use "bob" with the following password to access the wiki site:

correcthorsebatterystaple

Please reach out if there are any issues and have a good day!

Alice

--boundary3--

--boundary2

Content-Type: application/pgp-keys
Content-Disposition: attachment; filename="pEpkey.asc"

-----BEGIN PGP PUBLIC KEY BLOCK-----

[...]

-----END PGP PUBLIC KEY BLOCK-----

--boundary2--
