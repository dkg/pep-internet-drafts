#!/usr/bin/make -f

NAME := draft-pep-general
REV := 02
DRAFT := $(NAME)-$(REV)

OUTPUTS = $(DRAFT).xml $(DRAFT).txt $(DRAFT).html

DATE := $(shell date +%Y%m%d)

# Programs
KRAMDOWN := kramdown-rfc2629
XML2RFC := xml2rfc --v3
HOUSEKEEPING := ../misc/scripts/housekeeping.sh

# Targets
all: tmp $(OUTPUTS)

tmp:
	mkdir -p .refcache; cp ../shared/references/tmp/*.xml .refcache/

$(DRAFT).xml: $(NAME).mkd \
	../shared/author_tags/volker_birk.mkd \
	../shared/author_tags/hernani_marques.mkd \
	../shared/author_tags/bernie_hoeneisen.mkd \
	../shared/references/isoc-btn.mkd \
	../shared/references/implementation-status.mkd \
	../shared/text-blocks/key-words-rfc2119.mkd \
	../shared/text-blocks/terms-intro.mkd \
	../shared/text-blocks/handshake.mkd \
	../shared/text-blocks/trustwords.mkd \
	../shared/text-blocks/tofu.mkd \
	../shared/text-blocks/mitm.mkd \
	../shared/text-blocks/implementation-status.mkd \
	../shared/ascii-arts/basic-msg-flow.mkd \
	# ../shared/ascii-arts/pep_id_system.mkd \
	# ../shared/author_tags/claudio_luck.mkd \
	# ../shared/author_tags/shelburn.mkd \
	# to match backslash at the end of the previous line
	$(KRAMDOWN) $< > $@

$(DRAFT).txt: $(DRAFT).xml
	$(XML2RFC) --text $<

$(DRAFT).html: $(DRAFT).xml
	$(XML2RFC) --html $<

# copy files to review folder
review: distclean tmp $(DRAFT).txt  $(DRAFT).html
	mkdir -p review ; \
	cp -p $(DRAFT).txt  review/$(DRAFT)-pre$(DATE).txt ; \
	cp -p $(DRAFT).html review/$(DRAFT)-pre$(DATE).html

publish:
	$(HOUSEKEEPING)

clean:
	rm -f $(OUTPUTS) metadata.min.js

distclean:
	rm -f -r $(NAME)-* .refcache metadata.min.js

.PHONY: clean all
