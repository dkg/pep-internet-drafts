---
coding: utf-8

title: "pretty Easy privacy (pEp): Mapping of Privacy Rating"
abbrev: "pretty Easy privacy (pEp) Rating"
docname: draft-pep-rating-00
category: info

stand_alone: yes
pi: [toc, sortrefs, symrefs, comments]
ipr: trust200902

author:
{::include ../shared/author_tags/hernani_marques.mkd}
{::include ../shared/author_tags/bernie_hoeneisen.mkd}

normative:
  RFC4949:
#   RFC5322:
  RFC7435:
  I-D.pep-general:


informative:
#   RFC4880:
#   RFC5321:
#   RFC7258:
#   I-D.pep-email:
  I-D.pep-trustwords:
#   I-D.pep-rating:
  I-D.pep-handshake:
  GNUnet:
    target: https://grothoff.org/christian/habil.pdf
    title: "The GNUnet System"
    author:
      name: Christian Grothoff
      ins: C. Grothoff
    date: 2017-10-07

#   I-D.pep-keysync:
{::include ../shared/references/isoc-btn.mkd}
{::include ../shared/references/implementation-status.mkd}

--- abstract

In many Opportunistic Security scenarios end-to-end encryption is
automatized for Internet users. In addition, it is often required to
provide the users with easy means to carry out authentication.

Depending on several factors, each communication channel to different
peers may have a different Privacy Status, e.g., unencrypted,
encrypted and encrypted as well as authenticated. Even each message from/to
a single peer may have a different Privacy Status.

To display the actual Privacy Status to the user, this document
defines a Privacy Rating scheme and its mapping to a traffic-light
semantics. A Privacy Status is defined on a per-message basis as well
as on a per-identity basis. The traffic-light semantics (as color rating) 
allows for a clear and easily understandable presentation to the user 
in order to optimize the User Experience (UX).

This rating system is most beneficial to Opportunistic Security
scenarios and is already implemented in several applications of pretty
Easy privacy (pEp).

--- middle

# Introduction

In many Opportunistic Security {{RFC7435}} scenarios end-to-end
encryption is automatized for Internet users. In addition, it is often
required to provide the users with easy means to carry out
authentication.

Depending on several factors, each communication channel to different
identities may have a different Privacy Status, e.g.

* unreliable

* encrypted

* encrypted and authenticated

* mistrusted

Even each message from or to a single peer may have a different
Privacy Status.

To display the actual Privacy Status to the user, this document
defines a Privacy Rating scheme and its mapping to a traffic-light
semantics, i.e., a mapping to different color codes as used in a
traffic-light:

* red

* yellow

* green

* no color

Note: While "yellow" color is used in the context of traffic-lights
(e.g., in North America), in other parts of the world (e.g., the UK)
this is generally referred to as "orange" or "amber" lights. For the
scope of this document, "yellow", "amber", and "orange" refer to the
same semantics.

A Privacy Status is defined on a per-message basis as well as on a
per-identity basis. The traffic-light semantics (as color rating)
allows for a clear and easily understandable presentation to the user
in order to optimize the User Experience (UX). To serve also
(color-)blind Internet users or those using monochrome displays, the
traffic light color semantics may also be presented by simple texts
and symbols for signaling the corresponding Privacy Status.

The proposed definitions are already implemented and used in
applications of pretty Easy privacy (pEp) {{I-D.pep-general}}. This
document is targeted to applications based on the pEp framework and
Opportunistic Security {{RFC7435}}. However, it may be also used in
other applications as suitable.

Note: The pEp {{I-D.pep-general}} framework proposes to automatize the
use of end-to-end encryption for Internet users of email and other
messaging applications and introduces methods to easily allow
authentication.


{::include ../shared/text-blocks/key-words-rfc2119.mkd}


{::include ../shared/text-blocks/terms-intro.mkd}

{::include ../shared/text-blocks/handshake.mkd}
{::include ../shared/text-blocks/trustwords.mkd}
{::include ../shared/text-blocks/tofu.mkd}
{::include ../shared/text-blocks/mitm.mkd}


# Per-Message Privacy Rating

## Rating Codes

To rate messages (cf. also {{pep-rating}}), the following 13 Rating
codes are defined as scalar values (decimal):

| Rating code | Rating label           |
|:------------|-----------------------:|
|         -3  | under attack           |
|         -2  | broken                 |
|         -1  | mistrust               |
|          0  | undefined              |
|          1  | cannot decrypt         |
|          2  | have no key            |
|          3  | unencrypted            |
|          4  | unencrypted for some   |
|          5  | unreliable             |
|          6  | reliable               |
|          7  | trusted                |
|          8  | trusted and anonymized |
|          9  | fully anonymous        |

## Color Codes

For an Internet user to understand what the available Privacy Status
is, the following colors (traffic-light semantics) are defined:

| Color code | Color label    |
|:-----------|---------------:|
|         -1 | red            |
|          0 | no color       |
|          1 | yellow         |
|          2 | green          |

## Surjective Mapping of Rating Codes into Color Codes

Corresponding User Experience (UX) implementations use a surjective
mapping of the Rating Codes into the Color Codes (in traffic light
semantics) as follows:

| Rating codes | Color code | Color label |
|:-------------|------------|------------:|
|     -3 to -1 |         -1 | red         |
|       0 to 5 |          0 | no color    |        
|            6 |          1 | yellow      |
|       7 to 9 |          2 | green       |

This mapping is used in current pEp implementations to signal the
Privacy Status (cf. {{current-software-implementing-pep}}).

## Semantics of Color and Rating Codes

### Red

The red color MUST only be used in three cases: 

* Rating code -3: A man-in-the-middle (MITM) attack could be detected.

* Rating code -2: The message was tempered with.

* Rating code -1: The user explicitly states he mistrusts a peer,
  e.g., because a Handshake {{I-D.pep-handshake}} mismatched
  or when the user learns the communication partner was attacked and might
  have gotten the corresponding secret key leaked.

### No Color

No color SHALL be shown in the following cases:

* Rating code 0: A message can be rendered, but the encryption status
  is not clear, i.e., undefined

* Rating code 1: A message cannot be decrypted (because of an error
  not covered by rating code 2 below).

* Rating code 2: No key is available to decrypt a message (because it
  was encrypted with a public key for which no secret key could be
  found).

* Rating code 3: A message is received or sent out unencrypted
  (because it was received unencrypted or there's no public key to
  encrypt a message to a recipient).

* Rating code 4: A message is sent out unencrypted for some of the
  recipients of a group (because there's at least one recipient in
  the group whose public key is not available to the sender).

* Rating code 5: A message is encrypted, but cryptographic parameters
  (e.g., the cryptographic method employed or key length) are
  insufficient.

### Yellow

* Rating code 6: Whenever a message can be encrypted or decrypted with
  sufficient cryptographic parameters, it's considered reliable. It
  is mapped into the yellow color code.

### Green

* Rating code 7: A message is mapped into the green color code
  only if a pEp handshake {{I-D.pep-handshake}} was
  successfully carried out.

By consequence that means, that the pEp propositions don't strictly
follow the TOFU (cf. {{RFC7435}}) approach, in order to avoid
signaling trust without peers verifying their channel first.

In current pEp implementations (cf. {{implementation-status}}) only
rating code 7 is achieved.

The rating codes 8 and 9 are reserved for future use in pEp
implementations which also secure meta-data (rating code 8), by using
a peer-to-peer framework like GNUnet {{GNUnet}}, and/or allow for
fully anonymous communications (rating code 9), where sender and
receiver don't know each other, but trust between the endpoints could
be established nevertheless.


# Per-Identity Privacy Rating

The same Color Codes (red, no color, yellow and green) as for messages
(cf. {{color-codes}}) MUST be applied for identities (peers), so that
a user can easily understand, which identities private communication
is possible with.

The green color code MUST be applied to an identity whom the pEp
handshake {{I-D.pep-handshake}} was successfully carried out
with.

The yellow color code MUST be set whenever a public key could be
obtained to securely encrypt messages to an identity, although a
MITM attack cannot be excluded.

The no color code MUST be used for the case that no public key is
available to engage in private communications with an identity.

The red color code MUST only be used when an identity is marked as
mistrusted.

\[\[ It's not yet clear if there are proper cases where it makes sense
     to set an identity automatically to the red color code, as it
     appears to be difficult to detect attacks (e.g., secret key
     leakage) at the other endpoint with certainty.  \]\]


# Security Considerations

\[\[ TODO \]\]


# Privacy Considerations

\[\[ TODO \]\]


# IANA Considerations

This document has no actions for IANA.


{::include ../shared/text-blocks/implementation-status.mkd}


# Acknowledgements

The authors would like to thank the following people who have provided
feedback or significant contributions to the development of this
document: Leon Schumacher and Volker Birk

This work was initially created by pEp Foundation, and then reviewed
and extended with funding by the Internet Society's Beyond the Net
Programme on standardizing pEp. {{ISOC.bnet}}

--- back

# Excerpts from the pEp Reference Implementation

This section provides excerpts of the running code from the pEp
reference implementation pEp engine (C99 programming language).

## pEp rating

From the reference implementation by the pEp foundation, src/message_api.h:

~~~~~~~~
      typedef enum _PEP_rating {
          PEP_rating_undefined = 0,
          PEP_rating_cannot_decrypt,
          PEP_rating_have_no_key,
          PEP_rating_unencrypted,
          PEP_rating_unencrypted_for_some,
          PEP_rating_unreliable,
          PEP_rating_reliable,
          PEP_rating_trusted,
          PEP_rating_trusted_and_anonymized,
          PEP_rating_fully_anonymous,

          PEP_rating_mistrust = -1,
          PEP_rating_b0rken = -2,
          PEP_rating_under_attack = -3
      } PEP_rating;
~~~~~~~~

# Document Changelog

\[\[ RFC Editor: This section is to be removed before publication \]\]

* draft-pep-rating-00:
  * \[\[ TODO: Add changes here \]\]

* draft-marques-pep-rating-03:
  * Updates terms and references; other minor changes

* draft-marques-pep-rating-02:
  * Add Privacy and IANA Considerations sections
  * Updated Terms

* draft-marques-pep-rating-01:
  * Update references
  * Minor edits

* draft-marques-pep-rating-00:
  * Initial version

# Open Issues

\[\[ RFC Editor: This section should be empty and is to be removed
     before publication \]\]

* Considering the SHA-1 attack, rating must be discussed when encryption is
  fine while the signature is not.
* Better explain usage of Color Codes in Per-Identity Privacy Rating
* Decide whether rating code scalars 6 and 7-9 should be raised to
  leave space for future extensions
* Add Security Considerations
* Add more source code excerpts to Appendix
* Add rating codes for secure cryptographic methods and parameters
  and reference them
