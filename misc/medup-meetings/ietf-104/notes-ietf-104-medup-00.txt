MEDUP -- Missing Elements for Decentralized and Usable Privacy
Non-WG meeting @ IETF-104, Prague (Tyrolka room, Thu 2019-03-28, 18:15-19:30)

  * Mailing list: medup@ietf.org; subscribe: https://www.ietf.org/mailman/listinfo/MEDUP

  * Agenda:
    * Opening / Agenda Bashing [chairs]
    * Introduction to MEDUP [chairs]
    * Privacy Threat Modeling [Iraklis Symeonidis (SnT / uni.lu)]
    * The Missing Element in the Room [Gabriele Lenzini (SnT / uni.lu)]
    * Introduction to pEp [Hernani]
    * Document Status
      * draft-birk-pep-03 [Bernie]
      * draft-birk-pep-trustwords-03 [Bernie]
      * draft-marques-pep-handshake-02 [Bernie]
      * draft-marques-pep-rating-01 [Bernie]
      * draft-marques-pep-email-02 / draft-luck-lamps-pep-header-protection-01 
        [Claudio]
    * Open Issues [Bernie, Hernani]
    * Next Steps [chairs]
    * Open Mic

  (Last change: 2019-03-28 // rev: 08)

  * Slides / Streams: https://pep.foundation/docs/ietf/104/medup/

  * Minutes
    * Opening / Agenda Bashing [chairs]
      * Chairs open the meeting including Note Well
      * No requests for agenda bashing
  
    * Introduction to MEDUP [chairs]
      * MEDUP is about of enhancements to application protocols for 
        decentralized usable privacy
      * RFC 8280 has identified and documented important principles in such as 
        Data Minimization, End-to-End and Interoperability in order to enable 
        access to Human Rights. While (partial) implementations of these 
        concepts are already available, today's applications widely lack 
        Privacy support that ordinary users can easily handle.
      * In MEDUP these issues are addressed based on Opportunistic Security 
        (RFC 7435) principles. Updates/usage clarifications to application 
        level protocols such as email and XMPP are in scope.
      * There are other groups doing work in the area, such as HRPC, PEARG, 
        DINRG 
      * MEDUP aims go go beyond just principles, i.e., specifications for 
        Running Code
  
    * Privacy Threat Modeling [Iraklis Symeonidis (SnT / uni.lu)]
      * Different users might have different privacy needs 
        (journalists/whistleblowers, average users, corporate users)
      * There might be different attackers, from local attackers to global ones
        (global adversaries)
      * Different threats like detectability, linkability, identifiability, 
        non-repudiation and others are to to be considered
      * Also legal frameworks exist which demand for concepts like data 
        minimization (e.g., GDPR)
  
    * The Missing Element in the Room [Gabriele Lenzini (SnT/uni.lu)]
      * The missing link for privacy to work is actual usability
      * The missing element in the room is the average Internet user, which is 
        not able to use the tools which exist
      * Real user has a persona that may be influenced
      * Psychology models should be considered (it's not the right approach to 
        treat users as dummies).
      * Q&A
        * dkg 
          * Asks for examples of user interaction work integrated in network 
            protocols
          * Supports statements of the presentation:
            * Protocols should consider also the user; at the IETF we have 
              never traditionally done that
            * Doesn't know how to move IETF towards UX considerations
            * Expects a lot of pushback within the IETF
          * Thinks pushback is wrong, but it is just a fact
          * Asks for pointer to existing hooks that we could use out of places 
            like this
        * Gabriele (response to dkg)
          * Definitely from experients they have done, there are certain 
            guidelines
          * For example interrupting the user's primary goal, going somewhere 
            with warnings asking him to a take a decision, has proven to just 
            bothering the user
          * As he wants to reach his primary goal and he doesn't care about 
            whatever is in the middle
          * Condiering UX in the protocal to be made a principle matter of 
            design
          * E.g., move user decisions to policy (e.g., HSTS) are good move; 
            asking a user whether he/she trusts a web certificate was 
            unnecessary
        * dkg
          * Summarizes: the simplest answer is not to offer any explicit 
            controls to the user if you don’t have to
        * Gabriele:
          * If you do have to, you have to understand the psychology of the 
            user, in such a way you’re not imagining him in doing what you 
           want, but you consider that behavior as part of your system

    * Introduction to pEp [Hernani]
      * Aim to make text communications (i.e., email, chat, ...) private by 
        default
      * Most users are unable to use existing encryption tools like GnuPG
        (properly)
        * Need to fix this usability challenge by automation
        * Not just “good”, but easy privacy
      * The pEp architecture consists of several building blocks
      * Existing RFCs are used whenever available (and usable)
      * Some pieces are currently missing (or incomplete)
      * pEp intends to document the missing pieces in the IETF
      * Running Code: www.pep.software
  
    * Document Status
  
      * draft-birk-pep-03 [Bernie]
        * Main document
        * Handling of crypto is traditionally seen as difficult and hinders 
          widespread adoption
        * Missing pieces for easy decentralized useable privacy
        * Items relevant for all applications (email, chat, etc.)
  
      * draft-birk-pep-trustwords-03 [Bernie]
        * IANA registration of Trustwords
        * Mapping of hexadecimal stings to natural language words
        * Public registration of trustwords in different languages
  
      * draft-marques-pep-handshake-02 [Bernie]
        * Define easy authentication process for communication partners
        * Establishing trust relationship based on public keys using trustwords
  
      * draft-marques-pep-rating-01 [Bernie]
        * Easy understandable representation of Privacy Status
        * Description of different rating codes to inform a user on the Privacy
          Status
  
      * draft-marques-pep-email-02 / draft-luck-lamps-pep-header-protection-01
        [Claudio]
        * Define missing pieces for email
        * Message Format 2 is wrapping messsages to be able to process those
        * There are backward compatibility issues to be considered
  
      * Questions / Discussion
        * Sara
          * Wants to see what elements need more reasearch and shouldn't 
            initially be part of standardization process
          * Need to look at the distinctions of the layers between pure 
            protocol specification, user interaction and usability; these are 
            three distinct areas that in some cases are quite overlapped and 
            that can possibly get better attention by being unpicked
          * There are many protocols that are disguised here, not just email
          * Parts that aren’t directly related to protocol specicifiactions 
            could move out to research
          * Interested in a conversation what active research areas are in 
            place to work on
          * Some of these elements could benefit from coming to for example 
            PEARG and then feed back for changes in the protocols
          * It's fascinating work, it's pushing what we do at IETF, very much,
            at bit of care in choosing where it gets done could lead to much
            better reception of the work
          * If it is framed in the right way, we could be more receptive to it
        * Bernie 
          * Proponents are open to dicuss this
          * Invites to send a summary to the MEDUP mailing list for discission,
            to better understand how we can address this matter
        * dkg
          * Fascinating large amount of work that is presented here
          * In LAMPS mentioned the course of the deployed base; is the 
            fundamental problem we struggle here at IETF
          * Even if we can get everybody to switch today, I still wanna read my 
            own mail, which don't adhere to that specification
          * And we’re not getting everybody to switch, just because of the 
            deployed base
          * Would be interested in thinking about what the usability issues are
            and what the usability approaches are for dealing with 
            interoperability with non-compliant partners
          * While we know what is the right thing to do for generation
            [outgoing], and yet we are regularly obliged to consume [incoming]
            non-compliant data
          * That's a very tough question
          * Wants to hear the general ideas about how to address that
        * Hernani
          * The pEp project has a lot of experience in the field
          * Not yet clear how big the problem really is
          * Most people aren't encrypting at all
          * Far less than 1% actually use encryption at this time
          * Interested of documenting these issues in general, maybe in MEDUP
          * Independent of pEp, as this affects all projects in this area,
            including pEp or Autocrypt
          * Invites particularly also Autocrypt folks to engage with MEDUP
        * dkg
          * Need to find anyone to catalog the most common ways email is broken 
            (observations and recommendations to go forward)
        * Hernani
          * MEDUP discussions should not be just about pEp
          * Should be open for other approaches
        
        * Gabriele
          * Rating draft: rating model should be multidimensional (not linerar) 
        * Hernani
          * pEp is open to change protocols [if there are better ways to 
            achieve the goals]

    * Open Issues [Bernie, Hernani]
        * Will not discuss all issues on the slides due to time constraints
        * Slide 1/5: Should the very 1st message (TOFU, no key avaialble for 
          encryption) be signed?
          * What is the added value?
          * Does it lead to false peception of security?
          * Christian:
            * Depends, sometime repudiation is wanted
            * There might be a value for signing, e.g., sometimes signing 
              metadata may be needed (not the message itself)
        * Slide 2/5: Trustwords, what is the best charset?
          * ASCII 7bits, UTF-8, HTML-like encoding, IDNA, ...?
          * Normalization of UTF-8 was proposed on the mailing list (similar 
            sounding words map to the same UTF-8 representation)
          * [Discussion deviated and cut by chair due to time constraints]
  
    * Next Steps [chairs]
      * pEp email I-D will be updated
      * pEp key synchronization I-D is planned to be published
      * Continue discussion on MEDUP mailing list
      * Continue to hold non-WG meetings
      * Request BoF when ready
    
    * Open Mic
