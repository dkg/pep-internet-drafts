MEDUP agenda, IETF-104, Prague (Tyrolka room, Thu 2019-03-28, 18:15-19:30)

* Opening / Agenda Bashing [chairs]
* Introduction to MEDUP [chairs]
* Privacy Threat Modeling [Iraklis Symeonidis (SnT / uni.lu)]
* The Missing Element in the Room [Gabriele Lenzini (SnT / uni.lu)]
* Introduction to pEp [Hernani]
* Document Status
  o draft-birk-pep-03 [Bernie]
  o draft-birk-pep-trustwords-03 [Bernie]
  o draft-marques-pep-handshake-02 [Bernie]
  o draft-marques-pep-rating-01 [Bernie]
  o draft-marques-pep-email-02 / draft-luck-lamps-pep-header-protection-01 [Claudio]
* Open Issues [Bernie, Hernani]
* Next Steps [chairs]
* Open Mic

(Last change: 2019-03-28 // rev: 08)
