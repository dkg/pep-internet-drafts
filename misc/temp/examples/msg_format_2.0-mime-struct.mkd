Content-Type: multipart/mixed; boundary="54757bcc3eb452893caad660e3ec172"
--54757bcc3eb452893caad660e3ec172
  Content-Type: text/plain; charset="utf-8"
  Content-Disposition: inline; filename="msg.txt"
--54757bcc3eb452893caad660e3ec172
  Content-Type: message/rfc822; forwarded="no"

  Content-Type: multipart/mixed; boundary="38f7f3ba1fdc2fec532e637b1a575513"
  --38f7f3ba1fdc2fec532e637b1a575513
    Content-Type: multipart/alternative; boundary="342cbc2647732c5751b9bca7471403"
    --342cbc2647732c5751b9bca7471403
      Content-Type: text/plain; charset="utf-8"
      Content-Transfer-Encoding: quoted-printable
    --342cbc2647732c5751b9bca7471403
      Content-Type: text/html; charset="utf-8"
      Content-Transfer-Encoding: quoted-printable
    --342cbc2647732c5751b9bca7471403--
  --38f7f3ba1fdc2fec532e637b1a575513
    Content-Type: application/json
    Content-Disposition: attachment; filename="results.json"
  --38f7f3ba1fdc2fec532e637b1a575513--
--54757bcc3eb452893caad660e3ec172
  Content-Type: application/pgp-keys
  Content-Disposition: attachment; filename="pEpkey.asc"
--54757bcc3eb452893caad660e3ec172--
