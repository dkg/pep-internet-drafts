
   └─╴application/pkcs7-mime smime-type="enveloped-data"
    ↧ (decrypts to)
    └─╴application/pkcs7-mime smime-type="signed-data"
     ⇩ (unwraps to)
     └┬╴multipart/mixed ← Cryptographic Payload
      ├┬╴multipart/alternative
      │├─╴text/plain
      │└─╴text/html
      └─╴text/x-diff ← attachment          



   └─╴application/pkcs7-mime smime-type="enveloped-data"
    ↧ (decrypts to)
    └─╴application/pkcs7-mime smime-type="signed-data"
     ⇩ (unwraps to)
     └┬╴message/rfc822 ← Cryptographic Payload
      └┬╴multipart/mixed
       ├┬╴multipart/alternative
       │├─╴text/plain
       │└─╴text/html
       └─╴text/x-diff ← attachment          


   └─╴application/pkcs7-mime smime-type="enveloped-data"
    ↧ (decrypts to)
    └─╴application/pkcs7-mime smime-type="signed-data"
     ⇩ (unwraps to)
     └┬╴multipart/mixed ← Cryptographic Payload    
      ├─╴text/plain ← Legacy Display Part
      └┬╴message/rfc822
       └┬╴multipart/mixed
        ├┬╴multipart/alternative
        │├─╴text/plain
        │└─╴text/html
        └─╴text/x-diff ← attachment          
