#! /bin/bash

#############################################################################
### This script is used during the publication process of an Internet Draft.
### It ensures the following tasks are done (some manually, some automatic):
### - Build Internet-Drafts (for submission)
### - Submit to IETF (manually)
### - Move files to archive and add them to repository
### - update *.mkd and Makefile (revision number)
### - commit everything
###
### It has to be executed form the directory where the Makefile is.
###
### Disclairmer: This script is not totally fools-prove and may contain bugs
###
### Author: Bernie Hoeneisen
### Created: 2019-11-04 / Last Update: 2019-11-04
###
### (c) 2019-2020 / pEp Foundation
#############################################################################

## update from repository
echo
echo "Update from Repository..."
read -ep "Do you want to pull from repository now? (Y/n) ";

if [[ $REPLY != "N" ]] && [[ $REPLY != "n" ]]; then

    git pull

else
    echo "Note: Not updating from repository."
fi


## building Internet-Drafts
echo
echo "Build Internet-Drafts..."
read -ep "Do you want to build the Internet-Draft now? (Y/n) ";

if [[ $REPLY != "N" ]] && [[ $REPLY != "n" ]]; then

    make distclean
    make tmp # REMOVE later
    make

else
    echo "Note: Not building Internet-Draft"
fi

## Submit / Confirm Submission to IETF
echo
echo "If build was successful, please submit Internet-Draft now at" 
echo "https://datatracker.ietf.org/submit/ (unless you have done so before)!"
read -ep "Confirm Internet-Draft has been submitted successfully to IETF (y/N) ";

if [[ $REPLY != "Y" ]] && [[ $REPLY != "y" ]]; then

    echo "Aborting..."
    echo "You may try again."
    exit

fi

## Get essentials from Makefile
echo
echo "Verify docname and revision..." 
revision=`egrep -e "REV\s*\:\=" Makefile | cut -f 2 -d\= | sed 's/[ ]*//g'`
docname=`egrep -e "NAME\s*\:\=" Makefile | cut -f 2 -d\= | sed 's/[ ]*//g'`
new_revision=0$((10#$revision+1)) # 10 for decimal interpretation
new_revision=${new_revision: -2} # only for case if revsion >= 10

echo "docname file is '$docname'"
echo "current revision is '$revision'"
echo "New revision is '$new_revision'"

read -ep "Is the information in the last 3 lines correct? (y/N)"

if [[ $REPLY != "Y" ]] && [[ $REPLY != "y" ]]; then

    echo "Aborting..."
    echo "You may fix your Makefile and try again."
    exit

fi

arch="archive"

filename_mkd="$docname.mkd"

filename_xml="$docname-$revision.xml"
filename_txt="$docname-$revision.txt"
filename_html="$docname-$revision.html"


## Check files do exist
echo "Checking files for existance/non-existance..."

if [ ! -f "$filename_mkd" ]; then
    echo "$filename_mkd does not exist. Aborting..."
    exit
fi

if [ ! -f "$filename_xml" ]; then
    echo "$filename_xml does not exist. Aborting..."
    exit
fi

if [ ! -f "$filename_txt" ]; then
    echo "$filename_txt does not exist. Aborting..."
    exit
fi

if [ ! -f "$filename_html" ]; then
    echo "$filename_html does not exist. Aborting..."
    exit
fi

## Verify files do not exist

if [ -f "$arch/$filename_xml" ]; then
    echo "$arch/$filename_xml exists already. Aborting..."
    exit
fi

if [ -f "$arch/$filename_txt" ]; then
    echo "$arch/$filename_txt exists already. Aborting..."
    exit
fi

if [ -f "$arch/$filename_html" ]; then
    echo "$arch/$filename_html exists already. Aborting..."
    exit
fi

## Archive documents

echo "Move documents to archive folder..."
mkdir -p $arch
mv $filename_xml $filename_txt $filename_html $arch


## Rename Version in mkd and Makefile
echo
echo "Renaming version in mkd and Makefile..."
sed -i -e "s/docname\:[ ]*$docname-$revision[ ]*/docname\:\ $docname-$new_revision/g" $filename_mkd
sed -i -e "s/REV[ ]*\:\=[ ]*$revision[ ]*/REV\ \:\=\ $new_revision/g" Makefile

echo "Making diff..."
git diff $filename_mkd Makefile 

read -ep "Does that above look good for commit (y/N) ";

if [[ $REPLY != "Y" ]] && [[ $REPLY != "y" ]]; then

    echo "Aborting..."
    echo "Please fix $filename_mkd and/or Makefile manually and commit"
    exit

fi

## Update from Repository (again) before commit
echo
echo "Update from Repository (again) before commit..."
read -ep "Do you want to pull from repository before commit? (Y/n) ";

if [[ $REPLY != "N" ]] && [[ $REPLY != "n" ]]; then

    git pull

else
    echo "Note: Not updating from repository (again)."
fi

## Commit
echo
echo "Commiting to repository..."
read -ep "Do you want to commit the changes to the repository? (Y/n) ";

if [[ $REPLY != "Y" ]] && [[ $REPLY != "y" ]]; then

    echo "Aborting..."
    echo "Please add / commit / push manually."
    exit

fi
git add $arch/$filename_xml $arch/$filename_txt $arch/$filename_html
git commit -m "new revision published / housekeeping" $filename_mkd Makefile $arch/$filename_xml $arch/$filename_txt $arch/$filename_html
git push


### Notes
# make clean; make
# submit to IETF
# sucessful (y/n)
# move txt, html, xml files to archive
# git add those files
# get version number and doc name from Makefile
# update version number in Makefile
# update version number in mkd (only the header section line docname: draft-...)
# - note: white space after ":" may vary
# hg diff Makefile *.mkd
# look good (y/n)
# git pull
# git commit -m "new revision published / housekeeping"
# git push

