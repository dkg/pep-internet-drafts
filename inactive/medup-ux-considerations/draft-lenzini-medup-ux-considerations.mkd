---
coding: utf-8

title: "User Interaction Considerations in Protocols"
abbrev: User Interaction Considerations
docname: draft-lenzini-medup-ux-considerations-00
category: std

stand_alone: yes
pi: [toc, sortrefs, symrefs, comments]

author:
{::include ../shared/author_tags/gabriele_lenzini.mkd}
{::include ../shared/author_tags/borce_stojkovski.mkd}

normative:
#  RFC3156:
  RFC4949:
#  RFC5322:
  RFC7435:
#  I-D.birk-pep: 
#  I-D.marques-pep-handshake:
#  I-D.marques-pep-rating:
informative:
#  RFC4880:
#  RFC6973:
#  RFC5321:
#  RFC7258:
#  RFC7942:
#  RFC8280:
#  I-D.luck-lamps-pep-header-protection:
#  I-D.pep-email:
#  I-D.birk-pep-trustwords:
#  I-D.pep-keysync:

{::include ../shared/references/isoc-btn.mkd}

--- abstract

\[\[ TODO \]\]


--- middle

# Introduction

\[\[ TODO \]\]


{::include ../shared/text-blocks/key-words-rfc2119.mkd}

<!--
{::include ../shared/text-blocks/terms-intro.mkd}

{::include ../shared/text-blocks/handshake.mkd}
{::include ../shared/text-blocks/trustwords.mkd}
{::include ../shared/text-blocks/tofu.mkd}
{::include ../shared/text-blocks/mitm.mkd}
-->


# ----------------------------------------------------------------------------------------------------


# REAL TITEL

HERE COMES THE REAL TEXT....

\[\[ TODO \]\]

# ----------------------------------------------------------------------------------------------------


# Security Considerations

\[\[ TODO \]\]

# Privacy Considerations

\[\[ TODO \]\]

# IANA Considerations

This document has no actions for IANA.

# Acknowledgements

The authors would like to thank the following people who have provided
feedback or significant contributions to the development of this
document: \[\[ TODO \]\]

This work was initially created by pEp Foundation, and will be reviewed and
extended with funding by the Internet Society's Beyond the Net Programme on
standardizing pEp. {{ISOC.bnet}}

--- back

# Document Changelog

\[\[ RFC Editor: This section is to be removed before publication \]\]

* draft-lenzini-medup-ux-considerations-00:
  * Initial version

# Open Issues

\[\[ RFC Editor: This section should be empty and is to be removed
     before publication \]\]
